Projectile object for the godot game engine with support for bouncing projectiles and very high projectile speeds.
This pack also includes a tweakable armour object that can interact with projectiles and can be placed on most other nodes.
Make bouncy bullets by enabling "bounce" or "always bounce". Bullets with the bounce flag enabled will bounce on colliders with the "bouncy" group.
Make bullets that pass through objects with the piercing or always pierce flag. If something makes the bullet bounce, it'll be overriden; otherwise the bullet will pass through objects as long as "hits" isn't 0
If you found this useful, you can support me at https://www.patreon.com/kyrart or https://ko-fi.com/kydoart.