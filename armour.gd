extends Area
export (NodePath) var bodyPath = ".."
onready var body = get_node(bodyPath)

export (float) var health = 100
export (float) var damage_absorption_percent = 0.9
var damage_absorption

func _ready():
	add_to_group("armour", true)
	damage_absorption = 1 - damage_absorption_percent

func bullet_hit(bullet_damage, impulse,armour_piercing):
	if !armour_piercing:
		health -= bullet_damage/2
		body.bullet_hit(bullet_damage*damage_absorption, impulse*2,armour_piercing)
		pass
	else:
		health -= bullet_damage
		if body.has_method("bullet_hit"):
			body.bullet_hit(bullet_damage, impulse,armour_piercing)
			pass

func _process(_delta):
	if health <= 0:
		queue_free()
