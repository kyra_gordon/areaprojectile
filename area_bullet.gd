extends Area

export (float) var speed = 300
export (float) var damage = 10
export (float) var impulse = 0.1
onready var timer = $"Timer"
export (bool) var armour_piercing = false
export (int) var max_hits = 1
var hits = 0
var direction = Vector3()
onready var raycast = $"raycast".shape
var last_location = Vector3()
export (float) var lifespan = 1
export (bool) var bouncy = false#bounce off of bouncy objects
export (bool) var always_bounce = false#bounce regardless of hit object bounciness 
export (bool) var piercing = false#if the projectile passes through "soft" objects. if it doesnt bounce
export (bool) var always_pierce = false#always go through. superseded by bounce
export (float) var hit_speed_falloff = .75#the percentage of speed retained at each hit
export (float) var min_speed = .5#the speed at which the projectile deletes itself
var hit_bodies = []
var start = Vector3()
var vel

func _physics_process(delta):
	projectile(delta)
	pass

func _ready():
	add_to_group("projectile",true)
	start = global_transform.origin
	timer.start(lifespan)
	last_location = global_transform.origin
	min_speed = speed*min_speed
	pass

func projectile(delta):
	if speed <= min_speed:
		destroy()
	if !hit_bodies.empty():
		hit()
	last_location = global_transform.origin
	global_translate((global_transform.basis.z.normalized()*-1) * speed * delta)
	vel = translation-last_location 
	raycast.length = global_transform.origin.distance_to(last_location)
	pass

func _on_Timer_timeout():
	destroy()
	pass

onready var bullet_tip = $"bullet_tip"

func hit():
	impulse = (global_transform.basis.z.normalized()*-1)*impulse
	var closest_thing = get_closest()
	hits += 1
	if closest_thing.has_method("bullet_hit"):
		closest_thing.bullet_hit(damage, impulse,armour_piercing)
	if hits == max_hits:
		destroy()
	if always_bounce or bouncy and closest_thing.is_in_group("bounce"):
		var result = get_world().direct_space_state.intersect_ray(start, translation+bullet_tip.translation, [self])
		if result.size()>0: #if the raycast succeeds and returns a result dict
			#var rot = vel.bounce(result.normal)#reflect the velocity
			#look_at(translation+rot,Vector3.UP)#rotate the projectile the right way
			look_at(translation+(vel.bounce(result.normal)),Vector3.UP)
			start = translation#start coord used as a reference point in other stuff
			speed *= hit_speed_falloff#slow the bullet down when it bounces for verisimilitude
	elif piercing and !closest_thing.is_in_group("soft"):
		destroy()
	elif piercing and closest_thing.is_in_group("soft"):
		pass
	elif always_pierce:
		speed *= hit_speed_falloff*hit_bodies.size()
		if hit_bodies.has(closest_thing):
			hit_bodies.remove(hit_bodies.find(closest_thing))
		for i in hit_bodies:
			if i.has_method("bullet_hit"):
				i.bullet_hit(damage, impulse, armour_piercing)



func get_closest():
	var closest_dist
	var closest_thing
	for i in hit_bodies:
		if closest_dist == null:
			closest_dist = i.get_translation().distance_to(start)
		if i.get_translation().distance_to(start) <= closest_dist:
			closest_thing = i
			closest_dist = i.translation.distance_to(start)
	return closest_thing

func destroy():
	queue_free()
	pass

func _on_area_bullet_body_entered(body):
	if !body.is_in_group("projectile"):
		hit_bodies.append(body)
	pass


func _on_area_bullet_area_entered(area):
	if !area.is_in_group("projectile"):
		hit_bodies.append(area)
	pass


func _on_area_bullet_area_exited(area):
	if hit_bodies.has(area):
		hit_bodies.remove(hit_bodies.find(area))
	pass


func _on_area_bullet_body_exited(body):
	if hit_bodies.has(body):
		hit_bodies.remove(hit_bodies.find(body))
	pass
